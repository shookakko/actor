(in-package :actor)

(defvar *box-elements*
  '("grid-box" "vbox" "hbox"
    "zbox" "multibox" "gridbox"
    "cbox" "radio" "normalizer"
    "frame" "flat-frame" "tabs"
    "flat-tabs" "background-box" "scroll-box" "flat-scroll-box"
    "detach-box" "expander" "sbox" "split"))

(defclass handle ()
  ((type :initarg :type :accessor handle-type :type string)
   (attributes :initarg :attributes :accessor handle-attributes :type list)
   (dom-id :initarg :dom-id :accessor handle-id :type string)
   (dom-class :initarg :dom-class :accessor handle-class :type string)
   (iup-pointer :initarg :iup-pointer :accessor handle-pointer)
   (mapped :initarg :mapped :accessor handle-mapped :type boolean :initform nil)))

(defclass box (handle)
  ((root :initarg :root :accessor box-root-p :type boolean)
   (children :initarg :children :accessor box-children)))

(defclass element (handle)
  ())

(defun filter-element-type (type)
  (if (string-equal "scintilla" type)
      "iup-scintilla:scintilla"
      (format nil "iup:~a" type)))

;;TODO: Handle other iup packages
(defun make-box (&key children attributes type root id class pointer)
  (make-instance 'box
		 :type (filter-element-type type)
		 :dom-id id
		 :iup-pointer pointer
		 :dom-class class
		 :attributes attributes
		 :root root
		 :children children))

(defun make-element (&key attributes type id class pointer)
  (make-instance 'element
		 :type (filter-element-type type)
		 :dom-id id
		 :iup-pointer pointer
		 :dom-class class
		 :attributes attributes))
