(in-package :actor)

(defvar *virtual-dom* (make-hash-table :test #'equal)
  "Dynamic variable that saves all the available handlers sequentially.
When an element is created, it should be added with a unique keyword to this
variable, so it's accessible.")

(defvar *custom-attributes* '(:id :class :actor-list)
  "These are custom attributes that can be put into elements but are not mapped
into real iup attributes, are custom ones for actor usage.")

(defun custom-attribute-p (attribute)
  (member attribute *custom-attributes* :test #'equal))

(defun empty-virtual-dom ()
  "Utility function that clean the variable `*virtual-dom*'."
  (setf *virtual-dom* (make-hash-table :test #'equal)))

(defmethod add-dom-element ((handle handle))
  "Wrapper to add HANDLE to the dynamic variable `*virtual-dom*'."
  (let ((id (handle-id handle)))
    (if (null (gethash id *virtual-dom*))
	(prog1 (setf (gethash id *virtual-dom*) handle)
	  (log:info "DOM element of type ~a with ID ~a added."
		    (handle-type handle) (handle-id handle)))
	(error "There is already an element wit the ID ~a" id)))
  handle)

(defmethod map-dom-element ((element element))
  (add-dom-element element))

(defmethod map-dom-element ((box box))
  (add-dom-element box)
  (loop for child in (box-children box)
	do (map-dom-element child))
  box)

(defmethod map-dom-element :after (handle)
  (setf (handle-mapped handle) t))

;; xml = (xmls:parse (alexandria:read-file-into-string #P"/path/to/example.xml"))
(defun parse-xml-list (xml)
  "It gets a struct of type `xmls:node' and return a `box' object
with all the children handlers inside."
  (declare (type xmls:node xml))
  (let* ((name (xmls:node-name xml))
	 (attributes (xmls:node-attrs xml))
	 (id (find "id" attributes :test #'string= :key #'car)))
    (if (node-box-p xml)
	(make-box :type name
		  :root t
		  :class nil
		  :id (if id (second id)
			  (format nil "~a"
				  (uuid:make-v1-uuid)))
		  :attributes attributes
		  :children (parse-node xml))
	(error "The root node must have children."))))

(defun parse-node (node)
  (if (node-box-p node)
      (parse-node-box node)
      (parse-node-element (xmls:node-name node)
			  (xmls:node-attrs node))))

;;TODO: Optimize the class/id search (not to search two times)
(defun parse-node-box (box)
  (loop for node in (xmls:node-children box)
	for name = (xmls:node-name node)
	for attributes = (xmls:node-attrs node)
	for id = (find "id" attributes :test #'string= :key #'car)
	for class = (find "class" attributes :test #'string= :key #'car)
	if (node-box-p node)
	collect (make-box :type name
			  :id (if id (second id) (format nil "~a"
							 (uuid:make-v1-uuid)))
			  :class (second class)
			  :attributes attributes
			  :children (parse-node node)) 
	else
	collect (parse-node-element name attributes)))

(defun parse-node-element (name attributes)
  (let ((id (find "id" attributes :test #'string= :key #'car))
	(class (find "class" attributes :test #'string= :key #'car)))
    (make-element :type name
		  :id (if id (second id) (format nil "~a"
						 (uuid:make-v1-uuid)))
		  :class (second class)
		  :attributes attributes)))

(defun node-box-p (node)
  (declare (type xmls:node node))
  (member (xmls:node-name node) *box-elements* :test #'string=))

(defun validate-keyword (key value)
  (case key
    (t value)))

(defun string-to-symbol (value)
  (read-from-string (format nil "(quote ~a)" value)))

(defun keyword-check (key value)
  "Ensure that certain keywords are transform to their correct
representation, it also calls `validate-keyword' to validate
each keyword and their value before generating the IUP list."
  (list key
	(cond ((or (alexandria:ends-with-subseq "_CB" (string key))
		   (eq :k_any key)
		   (eq :action key))
	       (validate-keyword key (string-to-symbol value)))
	      (t value))))

(defun keywords-to-list (handle)
  "Transform the pair keywords to a list that it's translate to
a way that the iup wrapper can understand."
  (loop with keywords
	for (key value) in (handle-attributes handle)
	for pair = (keyword-check (read-from-string
				   (format nil ":~a" key))
				  value)
	unless (custom-attribute-p (car pair))
	do (progn (push (cadr pair) keywords)
		  (pushnew (car pair) keywords))
	finally (return keywords)))

(defmethod handle-to-iup :before (handle)
  (unless (handle-mapped handle)
    (restart-case (error 'map-handle-error :handle handle)
      (map-element () (map-dom-element handle)))))

(defmethod handle-to-iup ((element element))
  "It generate the code for the element IUP equivalent with the
attributes of ELEMENT. It also creates a binding for the object
to have a bidirectional map between the object and the pointer."
  `(let ((element (,(read-from-string (handle-type element))
		   ,@(keywords-to-list element)))) 
     ;;TODO: Move duplication code
     (setf (iup:attribute element "ID") ,(handle-id element)
	   (iup:attribute element "CLASS") ,(handle-class element)
	   (handle-pointer (gethash ,(handle-id element) actor::*virtual-dom*)) element)
     element))


(defun process-element (box children)
  (cond ((and children (string-equal (handle-type box) "iup:split"))
	 `(,(read-from-string (handle-type box))
	   ,(handle-to-iup (car children))
	   ,(handle-to-iup (second children))
	   ,@(keywords-to-list box)))
	((and children (member (handle-type box)
			       '("iup:scroll-box" "iup:radio"
				 "iup:flat-scroll-box"
				 "iup:sub-menu"
				 "iup:sbox")
			       :test #'string-equal))
	 `(,(read-from-string (handle-type box))
	   ,(handle-to-iup (car children))
	   ,@(keywords-to-list box)))
	(t
	 `(,(read-from-string (handle-type box))
	   ,(if children
		`(list ,@(mapcar #'handle-to-iup children))
		nil)
	   ,@(keywords-to-list box)))))

(defmethod handle-to-iup ((box box))
  "It generate the code for the element IUP equivalent with the
attributes of BOX and his children. It also creates a binding for the object
to have a bidirectional map between the object and the pointer."
  (let ((children (box-children box)))
    `(let ((element ,(process-element box children))) 
       (setf (iup:attribute element "ID") ,(handle-id box)
	     (iup:attribute element "CLASS") ,(handle-class box)
	     (handle-pointer (gethash ,(handle-id box) actor::*virtual-dom*)) element)
       element)))

(defun map-handle (handle)
  (if (gethash (handle-id handle) actor::*virtual-dom*)
      (eval (handle-to-iup handle))
      (error "Object ~a should be insert into the virtual doom before IUP mapping"
	     handle)))

;; OBJECT TO XML

(defun nodes-to-xml (nodes)
  (xmls:toxml nodes :indent t))

(defmethod handle-to-nodes ((box box))
  "Transform BOX into a xmls node struct."
  (let* ((type (subseq (handle-type box) (1+ (position #\: (handle-type box) :test #'char=))))
	 (children (loop for handle in (box-children box)
			 collect (handle-to-nodes handle))))
    (xmls:make-node :name type :ns nil :attrs (reverse (handle-attributes box)) :children children)))

(defmethod handle-to-nodes ((element element))
  "Transform ELEMENT into a xmls node struct."
  (let* ((type (subseq (handle-type element)
		       (1+ (position #\: (handle-type element) :test #'char=)))))
    (xmls:make-node :name type :ns nil :attrs (reverse (handle-attributes element)) :children nil)))


;; (sb-int:with-float-traps-masked
;;     (:divide-by-zero :invalid)
;;   (iup:with-iup () (iup:show (iup:dialog (eval *))) (iup:main-loop)) )

;; SCINTILLA
;; (sb-int:with-float-traps-masked
;;     (:divide-by-zero :invalid)
;;   (iup:with-iup () (iup-scintilla:open) (iup:show (iup:dialog (eval *))) (iup:main-loop)) )


;; ;;;; TEST ZONE
;; (defun get-obj ()
;;   (handle-to-iup (parse-xml-list(xmls:parse
;; 				 (alexandria:read-file-into-string #P"/home/fermin/quicklisp/local-projects/actor/src/example.xml")))))


;; (defun get-obj ()
;;   (parse-xml-list (xmls:parse
;; 		   (alexandria:read-file-into-string #P"/home/fermin/quicklisp/local-projects/actor/src/example.xml"))))

;; (defun ttest (handle)
;;   (declare (ignore handle))
;;   (let ((b (make-element :type "button"
;; 			 :id (uuid:make-v1-uuid)
;; 			 :attributes '(("title" "Hola que tal!")))))
;;     (insert-item (gethash "buenas" *virtual-dom*) b))
;;   iup:+default+)

;; (defun open-menu (handle)
;;   (declare (ignore handle))
;;   (let ((menu (gethash "main-menu" *virtual-dom*))
;; 	(center-box (gethash "center-box" *virtual-dom*)))
;;     (loop for child in (box-children menu)
;; 	  do (update-item-attr child "EXPAND" "NO"))
;;     (update-item center-box  '(("FLOATING" "NO") ("VISIBLE" "YES"))))
;;   iup:+default+)

;; (defun ttest (handle)
;;   (declare (ignore handle))
;;   (alexandria:write-string-into-file
;;    (nodes-to-xml (handle-to-nodes (gethash "root" *virtual-dom*)))
;;    #P"/tmp/test.xml")
;;   iup:+default+)


;; (defun ttest (handle)
;;   (declare (ignore handle))
;;   (let ((b (make-box :type "vbox"
;; 		     :id (uuid:make-v1-uuid)
;; 		     :children (make-element :type "button" :id (uuid:make-v1-uuid)))))
;;     (insert-item (gethash "main-menu" *virtual-dom*) b))
;;   iup:+default+)


;; (defun ttest (handle)
;;   (declare (ignore handle))
;;   ;;(swap-item (gethash "first" *virtual-dom*) (gethash "second" *virtual-dom*) )
;;   iup:+default+)
