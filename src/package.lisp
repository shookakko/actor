(defpackage actor
  (:use :cl)
  (:export
   ;;Virtual DOM variable
   #:*virtual-dom*

   ;; DOM functions
   #:map-dom-element

   ;; Classes
   #:handle
   #:box
   #:element
   #:make-box
   #:make-element

   ;; Handle parser methods/functions
   #:parse-xml-list
   #:handle-to-iup
   #:map-handle
   #:nodes-to-xml
   #:handle-to-nodes

   ;; Item methods
   #:map-item
   #:insert-item
   #:append-item
   #:update-item
   #:get-attribute

   ;; Utility functions
   #:defcallback
   #:defdialog
   #:empty-virtual-dom))



