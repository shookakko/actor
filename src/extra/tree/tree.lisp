(defcallback add-leaf (n &optional
			 (leaf-name "null")) nil
  (declare (ignore n))
  (let* ((arbol (gethash "arbol" *virtual-dom*))
	 (leaf-value (get-attribute arbol "VALUE"))
	 (leaf-command (format nil "ADDLEAF~a" leaf-value)))
    (update-item arbol `((,leaf-command ,leaf-name)))))

(defcallback add-branch (n &optional
			   (branch-name "null")) nil
  (declare (ignore n))
  (let* ((arbol (gethash "arbol" *virtual-dom*))
	 (leaf-value (get-attribute arbol "VALUE"))
	 (branch-command (format nil "ADDBRANCH~a" leaf-value)))
    (update-item arbol `((,branch-command ,branch-name)))))

(defcallback child-count (n) nil
  (declare (ignore n))
  (let* ((arbol (gethash "arbol" *virtual-dom*))
	 (output (gethash "output" *virtual-dom*)))
    (update-item output `(("APPEND" ,(get-attribute arbol "VALUE"))))))

