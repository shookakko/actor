(in-package :actor)

(defvar *templates* (make-hash-table :test #'equal))

(defstruct template
  (path nil :type pathname)
  (loaded nil :type boolean)
  (id nil :type string))

(defmethod insert-template ((template template))
  "Insert TEMPLATE into `*templates*'."
  (alexandria:ensure-gethash (template-id template) *templates* template))

(defun process-template (template parent &key (position :insert))
  (let ((template-box
	  (parse-xml-list
	   (xmls:parse
	    (alexandria:read-file-into-string template)))))
    (cond ((eq :insert position) (insert-item parent template-box))
	  ((eq :append position) (append-item parent template-box)))
    template-box))

(defgeneric load-template (template parent &key position use-templates)
  (:documentation 
   "Load TEMPLATE into PARENT using the POSITION insert or append.
It also has the option to insert the template into the dynamic variable
`*templates*'."))

(defmethod load-template ((template pathname) (parent handle) &key (position :insert)
								   (use-templates t))
  (let ((template-box
	  (process-template template parent :position position)))
    (when use-templates
      (insert-template (make-template :path template
				      :id (handle-id template-box)
				      :loaded t)))))

(defmethod load-template ((template template) (parent handle) &key (position :insert)
								   (use-templates t))

  (process-template (template-path template) parent :position position)
  (when use-templates
    (setf (template-loaded template) t)
    (insert-template template)))
