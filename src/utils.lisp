(in-package :actor)


(defmacro defcallback (name arguments continuation &body body)
  "Create a callback function with the name NAME and with the arguments ARGUMENTS.
It ensures that return a valid IUP callback, which can also be specify
with CONTINUATION."
  (unless continuation
    (setf continuation iup:+default+))
  (if (member (eval continuation) (list iup:+default+ iup:+close+
					iup:+ignore+ iup:+error+))
      `(defun ,name (,@arguments)
	 ,@body
	 ,continuation)
      (error "The continuation ~a is not valid" continuation)))

#+sbcl
(defmacro defdialog (name arguments &body body)
  "Create a function with name NAME and with arguments ARGUMENTS that
it's mean to have inside a IUP dialog."
  `(defun ,name (,@arguments)
     (sb-int:with-float-traps-masked
	 (:divide-by-zero :invalid)
       ,@body)))

#-sbcl
(defmacro defdialog (name arguments &body body)
  "Create a function with name NAME and with arguments ARGUMENTS that
it's mean to have inside a IUP dialog."
  `(defun ,name (,@arguments)
     ,@body))

#+sbcl
(defun test-xml-file (file)
  "Simple function to test actor XML FILE.
Empty the *virtual-doom* variable at the beginning and the end."
  (empty-virtual-dom)
  (sb-int:with-float-traps-masked
      (:divide-by-zero :invalid)
    (iup:with-iup ()
      ;;(iup-scintilla:open)
      (iup:show
       (iup:dialog
	(prog1 
	    (eval (handle-to-iup
		   (parse-xml-list
		    (xmls:parse
		     (alexandria:read-file-into-string file))))))))
      (iup:main-loop))
    (empty-virtual-dom)))

;; (defcallback ttest (button) ()
;;   (let ((dialog (iup:file-dialog :dialogtype "OPEN" :filter "*.xml")))
;;     (unwind-protect
;;          (progn
;;            (iup:popup dialog iup:+center+ iup:+center+)
;; 	   (load-template (pathname (iup:attribute dialog :value))
;; 			  (gethash "template_container" *virtual-dom*))
;;            (iup:message "Template added"
;;                         (format nil "Template ~A" (iup:attribute dialog :value))))
;;       (iup:destroy dialog))))
