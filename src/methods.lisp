(in-package :actor)

(defmethod get-parent ((handle handle))
  (gethash (iup:attribute (iup:get-parent (handle-pointer handle)) "ID")
	   *virtual-dom*))

(defmethod map-item :after (handle)
  (log:info "Box element ~a with ID ~a has been mapped."
	    (handle-type handle) (handle-id handle)))

(defmethod map-item ((box box))
  "Map BOX item and their children using iup:map and iup:refresh."
  (iup:map (handle-pointer box))
  (iup:refresh (handle-pointer box))
  (iup:refresh-children (handle-pointer box)))

(defmethod map-item ((element element))
  "Map ELEMENT item using iup:map and iup:refresh."
  (iup:map (handle-pointer element))
  (iup:refresh (handle-pointer element)))

(defmethod insert-item :after (item element)
  (log:info "Item ~a has been inserted in ~a."
	    (handle-id element)
	    (handle-id item)))

(defmethod insert-item ((box box) (element handle))
  "Insert ELEMENT into BOX, updating the `*virtual-dom*' reference."
  (unless (handle-mapped element)
    (map-dom-element element))
  (with-slots (dom-id children) box
    (map-handle element)
    (if children 
	(iup:insert (handle-pointer box) (handle-pointer
					  (car (last children)))
		    (handle-pointer element))
	(iup:insert (handle-pointer box) nil (handle-pointer element)))
    (setf (box-children box) (pushnew element (box-children box)))
    (map-item element)
    (map-item box)))

(defmethod append-item ((box box) (element handle))
  "Append ELEMENT into BOX, updating the `*virtual-dom*' reference."
  (unless (handle-mapped element)
    (map-dom-element element))
  (with-slots (dom-id children) box
    (map-handle element)
    (iup:append (handle-pointer box) (handle-pointer element))
    (setf (box-children box) (pushnew element (box-children box)))
    (map-item element)
    (map-item box)))

(defmethod update-item :after (handle attributes)
  (log:info "Attributes of ~a has been updated."
	    (handle-id handle)))

(defmethod update-item ((handle handle) (attributes list))
  "Update the HANDLE attributes from the ATTRIBUTE list, if the attribute doesn't exist
it is created and put into the list."
  (let ((handle-attributes (handle-attributes handle)))
    (loop for (key value) in attributes
	  unless (ignore-errors ; Maybe not the best way to do it?
		  (setf (cdr
			 (find key handle-attributes
			       :test #'string= :key #'first))
			value))
	  do (setf (handle-attributes handle)
		   (append handle-attributes
			   `((,key ,value))))
	  do (setf (iup:attribute (handle-pointer handle)
				  (string-upcase key))
		   value)
	  finally (map-item handle))))

(defmethod get-attribute ((handle handle) (attribute string))
  "Get the attribute ATTRIBUTE from HANDLE using iup:attribute."
  (iup:attribute (handle-pointer handle) attribute))


(defmethod delete-item ((element element))
  (iup:destroy (handle-pointer element)))

(defmethod delete-item ((box box))
  (iup:destroy (handle-pointer box))
  (loop for child in (box-children box)
	do (delete-item child)))

(defmethod delete-item :after (handle)
  (remhash (handle-id handle) *virtual-dom*)
  (setf (handle-pointer handle) nil
	(handle-mapped handle) nil)

  (log:info "Item type ~a with ID ~a has been deleted."
	    (handle-type handle)
	    (handle-id handle)))
