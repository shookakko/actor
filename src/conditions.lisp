(in-package :actor)

(define-condition map-handle-error (error)
  ((handle :initarg :handle :reader handle)))
