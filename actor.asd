
(eval-when (:compile-toplevel)
  (error "This ASDF file should be run interpreted."))

(defsystem "actor"
  :version "0.0.1"
  :author ""
  :license "GNU GPLv3"
  :depends-on (:xmls :alexandria :iup :log4cl)
  :components ((:module src
		:components
		((:file "package")
		 (:file "uuid")
		 (:file "classes")
		 (:file "utils")
		 (:file "parser")
		 (:file "methods")
		 (:file "conditions")
		 (:file "template"))))
  :description "")


(defsystem "actor/tests"
  :author ""
  :license "GNU GPLv3"
  :depends-on (:actor :fiveam)
  :components ((:module tests
		:serial t
		:components
		((:file "package")
		 (:file "tests"))))
  :description "Test system for actor")
